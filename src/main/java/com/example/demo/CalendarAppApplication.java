package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@ComponentScan(basePackages={ "com.wavelabs.resources", "com.wavelabs.service",
		"com.wavelabs.dao" })
@SpringBootApplication
@EnableSwagger2
public class CalendarAppApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(CalendarAppApplication.class, args);
	}
}
