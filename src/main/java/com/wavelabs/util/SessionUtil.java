package com.wavelabs.util;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * This contains the creation of Configuration, and building SessionFactory and
 * Sessions
 * 
 * @author tharunkumarb
 *
 */
public class SessionUtil {
	private final static Logger logger =Logger.getLogger(SessionUtil.class); 
	private static Configuration cfg = null;
	private static SessionFactory factory = null;
	private static Session session = null;
	private static int count = 0;

	/**
	 * This method provides only one SessionFactory, If SessionFactory is close
	 * it never creates new SessionFactory
	 * 
	 * @return SessionFactory
	 */

	public static SessionFactory getSessionFactory() {
		if (count == 0)
			try {
				buildSessionFactory();
			} catch (Exception e) {
				logger.info(e);
			}
		return factory;
	}

	@SuppressWarnings("deprecation")
	private static void buildSessionFactory() {
		try {
			cfg = new Configuration().configure();
			
			cfg = cfg.setProperty("hibernate.connection.url",
					System.getenv("database.url"));
			
			factory = cfg.buildSessionFactory();
			session = factory.openSession();
			count++;
		} catch (Exception e) {
			logger.info(e);
		}
	}

	/**
	 * This method provides a session object. If session is associated with
	 * sessionFactory it gives same session, if session is closed it gives new
	 * Session
	 * 
	 * @return Session
	 */

	public static Session getSession() {
		if (count == 0) {
			buildSessionFactory();
			count++;
		}
		if (!session.isOpen()) {
			session = factory.openSession();
		}
		return session;
	}

	/**
	 * This method provides Configuration object associated with SessionFactory,
	 * If sessionFactory not existing, it creates new SessionFacotry
	 * 
	 * @return Configuration
	 */

	public static Configuration getConfiguration() {

		if (count == 0) {
			buildSessionFactory();
			count++;
		}

		return cfg;

	}
}