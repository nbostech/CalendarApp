package com.wavelabs.resources.test;

import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.model.Receiver;
import com.wavelabs.resources.ReceiverResource;
import com.wavelabs.service.ReceiverService;
import com.wavelabs.service.test.DataBuilder;

@RunWith(MockitoJUnitRunner.class)
public class ReceiverResourceTest {

	@Mock
	ReceiverService receiverService;

	@InjectMocks
	private ReceiverResource receiverResource;

	@Test
	public void testPersistReceiver1() {
		Receiver receiver = DataBuilder.getReceiver();
		when(receiverService.createReceiver(any())).thenReturn(receiver);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = receiverResource.persistReceiver(receiver);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}
	
	@Test
	public void testPersistReceiver2() {
		Receiver receiver = DataBuilder.getReceiver();
		when(receiverService.createReceiver(any())).thenReturn(null);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = receiverResource.persistReceiver(receiver);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}

	
}